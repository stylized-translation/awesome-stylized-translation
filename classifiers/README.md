Получение предиктов классификаторов сказка/твиттер/новость/Лев Толстой:

```angular2html
clf = MulticlassClassifier(device="cpu")
predictions = clf.predict(
                list_of_sentences, 
                ['fairytale', 'twitter', 'news', 'tolstoy'],
                batch_size=64, 
                preprocessing_n_jobs=4
                ) #returns np.array
```
