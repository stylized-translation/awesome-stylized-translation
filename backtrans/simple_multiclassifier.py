import torch.nn as nn


class SimpleNet(nn.Module):
    def __init__(self, embed_size=256, hid_dim=128, class_count=5):
        super(SimpleNet, self).__init__()
  
        self.fc1 = nn.Linear(embed_size, hid_dim) 
        self.relu = nn.Relu()
        self.fc2 = nn.Linear(hid_dim, hid_dim // 2)
        self.fc3 = nn.Linear(hid_dim // 2, class_count)
        self.softmax = nn.Softmax()

    def forward(self, x):
        return self.fc3(self.relu(self.fc2(self.relu(self.fc1(x)))))
    
    def classify(self, x):
        logits = self.forward(x).detach()
        return self.softmax(logits)
        


    
