# Style Transfer Through Back-Translation (https://github.com/shrimai/Style-Transfer-Through-Back-Translation)

**General models and datasets.**

**Dowload the english--french and french--english models from the following link:**

http://tts.speech.cs.cmu.edu/style_models/english_french.tar

http://tts.speech.cs.cmu.edu/style_models/french_english.tar


**Dowload the trained gender, political slant and sentiment classifiers from the following link:**

http://tts.speech.cs.cmu.edu/style_models/gender_classifier.tar
http://tts.speech.cs.cmu.edu/style_models/political_classifier.tar
http://tts.speech.cs.cmu.edu/style_models/sentiment_classifier.tar


**Download the trained style models from the following links:**

http://tts.speech.cs.cmu.edu/style_models/female_generator.tar
http://tts.speech.cs.cmu.edu/style_models/male_generator.tar
http://tts.speech.cs.cmu.edu/style_models/democratic_generator.tar
http://tts.speech.cs.cmu.edu/style_models/republican_generator.tar
http://tts.speech.cs.cmu.edu/style_models/positive_generator.tar
http://tts.speech.cs.cmu.edu/style_models/negative_generator.tar

Place these models in the models/style_generators folder.