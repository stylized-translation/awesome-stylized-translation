import numpy as np
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE

def plot_tSNE(vbatch, labels, name = "", random_state = 42):
    """tSNE-plot for vbatch with labels. 
    
    tSNE have 2 components.

    Keyword arguments:
    vbatch -- array of [batch_size x vector_dimension] vectorized batch; 
    labels -- int array of [batch_size] class of each element in batch;
    random_state - int, state for TSNE;
    
    returns figure & axis
    """
    projected_batch = TSNE(n_components=2, random_state=random_state).fit_transform(vbatch)
    
    figure, axs = plt.subplots(nrows=1, ncols=1, figsize=(7, 7))
    
    unique_labels = np.unique(labels)
    for target in unique_labels:
        indicesToKeep = np.where(labels == target)[0]
        axs.scatter(projected_batch[indicesToKeep, 0], projected_batch[indicesToKeep, 1], s = 10)
    axs.grid()
    axs.legend(unique_labels)
    axs.set_title("tSNE" + name)
    
    return figure, axs

def plot_tSNE_and_save(vbatch, labels, savepath = "./tmp-tSNE.png", figname = "", random_state = 42):
    """utility method with saving option

    Keyword arguments:
    vbatch -- array of [batch_size x vector_dimension] vectorized batch; 
    labels -- int array of [batch_size] class of each element in batch;
    random_state - int, state for TSNE;
    """
    plot_tSNE(vbatch, labels, figname, random_state)
    plt.savefig(savepath)
