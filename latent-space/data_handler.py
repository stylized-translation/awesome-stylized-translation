from deeppavlov.core.common.file import read_json
from deeppavlov import build_model, configs

from utils import get_gdrive_wget_command

import torch
import torch.nn.functional as F
import os

#import fasttext
import pandas as pd
import numpy as np
import re

from tqdm.notebook import tqdm
import pickle

from torch.utils.data import Dataset, DataLoader

def tokenize(text):
    # text ='\n'.join(list(negative[3].values))
    text = text.lower()
    text.replace('ё', 'е')
    text = re.sub(r'[^а-я ]+', ' ', text)
    return text

file_pathes = {
    'model.bin' : '1CftjnReFEWlQOSh0hYqdLOE0AmbCXXzT',
    'tweets.csv' : '1jhcvRXD1b_AWpfUw2bmolc86_EPWbK6A',
    'bert/pytorch_model.bin' : '1drFh7Smr-8mmXsgMpaVDvINrdRruwPSt',
    'bert/vocab.txt' : '19M9kjD5n7tkIKBZNyShkC21tCljOUVE1',
    'bert/bert_config.json' : '1QFGwvP0UUFipMKNC1-BfGKEv6Q0claa6'
}

class TextHandler:
    '''
        Данный интерфейс-класс в основном нужен для инференса и валидации. 
        У нас модель такова, что на вход принимает эмбеддинги, а на выход
        выплевывает айдишники. Данный класс нужен для предобработки
        (то есть получения эмбеддингов), и для постобработки (склейки айдиншников в 
        готовые красивые предложения). 
    '''

    def __init__(self, 
                 pad_id_symbol, 
                 pad_emb_symbol, 
                 emb_type, 
                 info, 
                 emb_size): 
        self.pad_id_symbol = pad_id_symbol # чем паддятся айдишники токенов
        self.pad_emb_symbol = pad_emb_symbol # чем паддятся эмбединги 
        self.emb_type = emb_type
        self.info = info
        self.emb_size = emb_size

    def get_embeddings(self, str_array, with_token_ids=False, max_sentence_length=None):
        '''
        Arguments:
            str_array: массив (список) str, от которых нужно взять эмбеддинги
            max_sentence_length: int или None. Если None, то обрезать предложения не нужно
        Returns:
            tuple: embeddings, token_ids
        '''
        pass
    
    def transform_ids_to_str(self, ids):
        '''
        Arguments:
            ids: список списков айдишников токенов
        Returns:
            numpy array of strings --- массив склеенных предложений
        '''
        pass

    def get_vocab_size(self):
        '''
            Размер словаря --- количество всех возможных (саб)токенов
        '''
        pass

    def get_emb_size(self):
        return self.emb_size

    def get_emb_type(self):
        '''
            Вернуть тип эмбедингов (Bert, FastText, ELMO)
        '''
        return self.emb_type

    def get_emb_info(self): 
        ''' 
            Вернуть полную информацию про эмбеддинги (название, на чём обучили, откуда взяли)
        '''
        return self.info

    def get_pad_id_symbol(self):
        return self.pad_id_symbol
    
    def get_pad_emb_symbol(self):
        return self.pad_emb_symbol

class BertTextHandler(TextHandler):
    def __init__(self, 
                 save_model_to= 'bert', # загрузить модель из гугл драйва, если 
                                        # ЕЁ НЕТ В ДИРЕКТОРИИ save_model_to 
                 pad_emb_symbol=0, build=True):

        '''
            TODO: download if needed and initialize the deeppavlov bert
        '''
        
        if not os.path.isdir(save_model_to):
            os.mkdir(save_model_to)

        if not os.path.isfile(save_model_to + '/pytorch_model.bin'):
            print("XD")
            wget_command = get_gdrive_wget_command(file_id=file_pathes['bert/pytorch_model.bin'], 
                                                  save_to_path=save_model_to + '/pytorch_model.bin')
            os.system(wget_command)

        if not os.path.isfile(save_model_to + '/bert_config.json'):
            print("XD")
            wget_command = get_gdrive_wget_command(file_id=file_pathes['bert/bert_config.json'], 
                                                  save_to_path=save_model_to + '/bert_config.json')
            os.system(wget_command)

        if not os.path.isfile(save_model_to + '/vocab.txt'):
            print("XD")
            wget_command = get_gdrive_wget_command(file_id=file_pathes['bert/vocab.txt'], 
                                                  save_to_path=save_model_to + '/vocab.txt')
            os.system(wget_command)
        
        if build:
            bert_config = read_json(configs.embedder.bert_embedder)
            bert_config['metadata']['variables']['BERT_PATH'] = save_model_to
            bert_config['chainer']['out'] = ['subword_tokens', 'subword_emb']
            self.model = build_model(bert_config)

        vocab = open(save_model_to + '/vocab.txt', 'r', encoding='utf-8').read()
        self.w2ind = {b:a for a,b in enumerate(vocab.split())}
        self.ind2w = {a:b for a,b in enumerate(vocab.split())}
        
        super(BertTextHandler, self).__init__(pad_id_symbol=self.w2ind['[PAD]'], 
                                              pad_emb_symbol=pad_emb_symbol, 
                                              emb_type = 'Bert',
                                              info='deeppavlov bert downloaded from ...', 
                                              emb_size=768)
        
    def _subtokens_to_inds(self, subtokens):
        return [self.w2ind[subtoken] for subtoken in subtokens]

    def _inds_to_subtokens(self, idxes):
        return [self.ind2w[idx] for idx in idxes if idx != self.pad_id_symbol]

    def _join_bert_subtokens(self, out):
        text = ''

        for w in out:
            if w == '[PAD]':
                break
            if w  == '[CLS]':
                continue
            if w == '[SEP]':
                break
            if '#' == w[0]:
                text = ''.join([text, w[2:]])
            elif w in ['!', ',', '.', '?']:
                text = ''.join([text, w])
            else:
                text = ' '.join([text, w])
        return text[1:]

    def _to_max_len(self, tokens, embeddings, max_len):
        res_tokens = []
        res_embeddings = []
        for token, embedding in zip(tokens, embeddings):
            n_padding = max_len - len(token)
            token = F.pad(torch.tensor(token), 
                          pad=(0, n_padding), 
                          mode='constant', 
                          value=self.pad_id_symbol)
            
            embedding = F.pad(torch.tensor(embedding), 
                              pad=(0, 0, 0, n_padding), 
                              mode='constant', 
                              value=self.pad_emb_symbol)
            
            res_tokens.append(token)
            res_embeddings.append(embedding)
        return torch.stack(res_tokens), torch.stack(res_embeddings)

    def get_embeddings(self, str_array, max_sentence_length=None):
        tokens, embeddings = self.model(str_array)
        tokens = [self._subtokens_to_inds(x) for x in tokens]
        tokens = [token[1:] for token in tokens]
        embeddings = [embedding[1:] for embedding in embeddings]
        
        if max_sentence_length:
            tokens, embeddings = self._to_max_len(tokens, embeddings, max_sentence_length)
        
        return tokens, embeddings
    
    def transform_ids_to_str(self, ids):
        return [self._join_bert_subtokens(self._inds_to_subtokens(x)) for x in ids]

    def get_vocab_size(self):
        return len(self.w2ind)

from transformers import BertTokenizer, PreTrainedTokenizer 

class BertNoAttentionTextHandler(TextHandler):
    def __init__(self, 
                 save_model_to= 'bert', # загрузить модель из гугл драйва, если 
                                         # ЕЁ НЕТ В ДИРЕКТОРИИ save_model_to
                 pad_id_symbol=-2, 
                 pad_emb_symbol=0):
        super(BertNoAttentionTextHandler, self).__init__(pad_id_symbol, 
                                              pad_emb_symbol, 
                                              emb_type = 'Bert',
                                              info='deeppavlov bert first layer embeddings ', 
                                              emb_size=768)
        '''
            TODO: download if needed and initialize the deeppavlov bert
        '''
        if not os.path.isdir(save_model_to):
            os.mkdir(save_model_to)

        if not os.path.isfile(save_model_to + '/pytorch_model.bin'):
            wget_command = get_gdrive_wget_command(file_id=file_pathes['bert/pytorch_model.bin'], 
                                                  save_to_path=save_model_to + '/pytorch_model.bin')
            os.system(wget_command)

        if not os.path.isfile(save_model_to + '/bert_config.json'):
            wget_command = get_gdrive_wget_command(file_id=file_pathes['bert/bert_config.json'], 
                                                  save_to_path=save_model_to + '/bert_config.json')
            os.system(wget_command)

        if not os.path.isfile(save_model_to + '/vocab.txt'):
            wget_command = get_gdrive_wget_command(file_id=file_pathes['bert/vocab.txt'], 
                                                  save_to_path=save_model_to + '/vocab.txt')
            os.system(wget_command)
        
        self.tokenizer = BertTokenizer.from_pretrained('bert/', do_basic_tokenize=False)
        self.embeddings = torch.load('bert/pytorch_model.bin')['bert.embeddings.word_embeddings.weight']

        vocab = open(save_model_to + '/vocab.txt', 'r', encoding='utf-8').read()
        self.w2ind = {b:a for a,b in enumerate(vocab.split())}
        self.ind2w = {a:b for a,b in enumerate(vocab.split())}
        
    def _subtokens_to_inds(self, subtokens):
        return [self.w2ind[subtoken] for subtoken in subtokens]

    def _inds_to_subtokens(self, idxes):
        return [self.ind2w[idx] for idx in idxes if idx != self.pad_id_symbol]

    def _join_bert_subtokens(self, out):
        text = ''

        for w in out:
            if w == '[CLS]':
                continue
            if w == '[PAD]' or w == '[SEP]':
                break
            if '#' == w[0]:
                text = ''.join([text, w[2:]])
            elif w in ['!', ',', '.', '?']:
                text = ''.join([text, w])
            else:
               text = ' '.join([text, w])
        return text[1:]

    def get_embeddings(self, str_array, max_sentence_length=None):
        max_length = max_sentence_length + 1 if max_sentence_length is not None else None
        tokens = self.tokenizer.batch_encode_plus(str_array, pad_to_max_length=True, 
                                                  max_length=max_length)
        input_ids = torch.tensor(tokens['input_ids'])[:, 1:]
        embeddings = torch.cat([torch.cat([self.embeddings[ind].unsqueeze(0) for ind in text], 0).unsqueeze(0)
        for text in input_ids], 0)
        return input_ids, embeddings
    
    def transform_ids_to_str(self, ids):
        return [self._join_bert_subtokens(self._inds_to_subtokens(x)) for x in ids]

    def get_vocab_size(self):
        return len(self.w2ind)

def get_texts_and_labels(path, random_order=True): 
    csv = pd.read_csv(path)
    csv.dropna(inplace=True)

    texts, labels = np.array(csv.text, dtype=str), np.array(csv.label, dtype=int)

    if random_order:
        order = np.random.permutation(len(texts))
        texts, labels = texts[order], labels[order]
    
    return texts, labels

class NoAttentionBertDataset(Dataset):
    """Face Landmarks dataset."""

    def __init__(self, text_handler, csv_path, max_sentence_length=50, random_order=True):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """ 
        
        self.texts, self.labels = get_texts_and_labels(csv_path, random_order=random_order)
        self.text_handler = text_handler
        self.max_sentence_length = max_sentence_length
            
    def __len__(self):
        return len(self.texts)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()
        
        texts, labels = self.texts[idx], self.labels[idx]
        
        ids, embs = self.text_handler.get_embeddings(np.array([texts]), 
                                                     max_sentence_length=self.max_sentence_length)
        sample = {'BOI' : ids.squeeze(), 
                  'labels': torch.from_numpy(np.array([labels])).squeeze(),
                  'embeddings': embs.squeeze()}

        return sample    
    
def save_pickles(texts, labels, text_handler, save_to_path, batch_size=20, save_every=2000):
    N = len(texts)
    n_batches = int(np.ceil(float(N / batch_size)))
    data = {'BOI': [], 
           'embeddings': [], 
           'labels': []}
    pickle_it = 1

    for i in tqdm(range(n_batches)):
        start_range, end_range = i * batch_size, min(i * batch_size + batch_size, N)
        ids, embs = text_handler.get_embeddings(texts[start_range:end_range], max_sentence_length=50)
        cur_labels = torch.from_numpy(labels[start_range:end_range])
        data['BOI'].append(ids)
        data['embeddings'].append(embs)
        data['labels'].append(cur_labels)

        if end_range % save_every == 0 or end_range == N:
            data = {name: torch.cat(data[name], axis=0) for name in data} 

            file_path = os.path.join(save_to_path, 'data{0:003d}.pickle'.format(pickle_it))
            print('Saving at', end_range, 'iteration to', file_path)
            with open(file_path, 'wb') as f:
                pickle.dump(data, f)
            pickle_it += 1
            data = {'BOI': [], 'embeddings': [], 'labels': []}
            
def iterate_bucket(bucket, batch_size=64, random_order=True):
    assert bucket['BOI'].shape[0] == bucket['embeddings'].shape[0]
    assert bucket['BOI'].shape[0] == bucket['labels'].shape[0]
    
    N_bucket = bucket['BOI'].shape[0]
    if random_order:
        order = np.random.permutation(N_bucket)
        bucket = {name: bucket[name][order] for name in bucket}
    
    n_batches = int(np.ceil(float(N_bucket / batch_size)))
    for i in range(n_batches):
        #print(order[0])
        start_range, end_range = i * batch_size, i * batch_size + min(batch_size, N_bucket)
        yield {name: bucket[name][start_range:end_range] for name in bucket}

def iterate_pickles(path, batch_size=50, random_order=True):
    names = np.sort(os.listdir(path))
    if random_order:
        order = np.random.permutation(len(names))
        names = names[order]
    
    for pickle_file in names:
        with open(os.path.join(path, pickle_file), 'rb') as f:
            bucket = pickle.load(f)

        for batch in iterate_bucket(bucket, batch_size=batch_size, random_order=random_order):
            yield batch
