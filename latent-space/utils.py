import torch
import random
import os
import numpy as np
import pandas as pd

def infer_mask(ids, eos_ix):
    return ids != eos_ix

def get_bow_vector(input, vocab_size, eos_ix):
    result = torch.zeros(input.shape[0], vocab_size, device=input.device)
    for t in range(input.shape[1]):
        result[range(input.shape[0]), input[:, t]] += 1
    result[:, eos_ix] = 0
    result /= result.sum(dim=1, keepdims=True)
    return result

def set_random_seed(random_seed):
    torch.manual_seed(random_seed)
    torch.cuda.manual_seed_all(random_seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False
    np.random.seed(random_seed)
    random.seed(random_seed)
    os.environ['PYTHONHASHSEED'] = str(random_seed)
    
def transfer(texts, ae_model, text_handler, style=None, max_sentence_length = 50, device='cpu'):
    if style is not None:
        style = torch.tensor(style).type(torch.FloatTensor).to(device)

        if len(style.shape) == 1:
            style = style[None, :].repeat(len(texts), 1).to(device)

    ids, embs = text_handler.get_embeddings(texts, max_sentence_length=max_sentence_length)
    mask = infer_mask(ids, text_handler.get_pad_id_symbol()) 
    _, _, output = ae_model.to(device)(embs.to(device), mask.to(device), set_style=style)
    out_ids = torch.argmax(output, dim=1).cpu().detach().numpy()

    out_texts = text_handler.transform_ids_to_str(out_ids)
    
    return out_texts

def get_checkpoint_info(path):
    if os.path.isdir(path):
        experiment_path = path
        epoch_ids = [int(checkpoint.split('_')[1]) \
                     for checkpoint in os.listdir(os.path.join(path, 'checkpoints'))]
        if len(epoch_ids) == 0:
             raise Exception('No checkpoints in', path)
        epoch = max(epoch_ids)

        checkpoint_path = str()
        for checkpoint in os.listdir(os.path.join(path, 'checkpoints')):
            if epoch == int(checkpoint.split('_')[1]):
                checkpoint_path = os.path.join(path, 'checkpoints', checkpoint)
                break
    elif os.path.isfile(path):
        checkpoint_path = path
        epoch = int((path.split('/')[-1]).split('_')[1])
        experiment_path = os.path.split(os.path.split(path)[0])[0]
    else:
        raise Exception(str(path) + ' is neither the path to the experiment nor to the epoch checkpoint')
    
    iter_logs_csv = pd.read_csv(os.path.join(experiment_path, 'logs', 'iter_logs.csv'))
    epoch_logs_csv = pd.read_csv(os.path.join(experiment_path, 'logs', 'epoch_logs.csv'))
    iter_id = max(iter_logs_csv[iter_logs_csv['epoch'] == epoch].iteration)

    return epoch, iter_id, experiment_path, checkpoint_path, \
           iter_logs_csv[iter_logs_csv['epoch'] <= epoch], \
           epoch_logs_csv[epoch_logs_csv['epoch'] <= epoch]

def get_gdrive_wget_command(file_id, save_to_path):
    wget_command = 'wget --load-cookies /tmp/cookies.txt '\
                   '\"https://docs.google.com/uc?export=download'\
                   '&confirm=$(wget --quiet --save-cookies '\
                   '/tmp/cookies.txt --keep-session-cookies '\
                   '--no-check-certificate '\
                   '\'https://docs.google.com/uc?export=download&id={}\' '\
                   '-O- | sed -rn \'s/.*confirm=([0-9A-Za-z_]+).*/\\1\\n/p\')'\
                   '&id={}\" -O {} && rm -rf /tmp/cookies.txt'.format(file_id, file_id, save_to_path)
    
    return wget_command