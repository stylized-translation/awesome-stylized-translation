import numpy as np
import pandas as pd
import os,sys,inspect
from data_handler import BertTextHandler, save_pickles
from utils import get_gdrive_wget_command
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--name', type=str, default=os.popen('whoami').read().strip())
parser.add_argument('--pad_symbol', type=int, default=1) # TODO: fix pad symbol

def get_texts_and_labels(path, random_order=True):
    csv = pd.read_csv(path)
    csv.dropna(inplace=True)

    texts, labels = np.array(csv.text, dtype=str), np.array(csv.positive, dtype=int)

    if random_order:
        order = np.random.permutation(len(texts))
        texts, labels = texts[order], labels[order]

    return texts, labels

def loadEmbedings(args):
    # hash of file from gdrive link
    #
    # Example:
    # link: https://drive.google.com/file/d/11GXyAhbE4jyW1DSnfXss5WcCOJE_xQ_H/view?usp=sharing
    # hash: 11GXyAhbE4jyW1DSnfXss5WcCOJE_xQ_H
    #
    train_csv_id = '1T9MLSLIIAfslcT_Od6CgEWPZg3hr70RY'
    val_csv_id = '11GXyAhbE4jyW1DSnfXss5WcCOJE_xQ_H'

    data_path = os.path.join("/data", args.name)
    embs_path = os.path.join(data_path, 'embeddings')

    os.mkdir(embs_path)

    train_csv_path = os.path.join(embs_path, 'train.csv')
    val_csv_path = os.path.join(embs_path, 'val.csv')

    for csv_id, path in [(train_csv_id, train_csv_path), (val_csv_id, val_csv_path)]:
        wget_command = get_gdrive_wget_command(file_id=csv_id, save_to_path=path)
        result_command = os.system(wget_command)
        print('Command\n', wget_command, '\nreturned', result_command)

    train_texts, train_labels = get_texts_and_labels(train_csv_path, random_order=True)
    val_texts, val_labels = get_texts_and_labels(val_csv_path, random_order=True)

    train_path = os.path.join(embs_path, "train")
    val_path = os.path.join(embs_path, "val")

    os.mkdir(train_path)
    os.mkdir(val_path)

    text_handler = BertTextHandler(save_model_to=os.path.join(data_path, 'bert'),
                               build=True, pad_emb_symbol=args.pad_symbol)

    save_pickles(train_texts, train_labels, text_handler, save_every=2000, save_to_path=train_path)
    save_pickles(val_texts, val_labels, text_handler, save_every=2000, save_to_path=val_path)


if __name__ == '__main__':
    args = parser.parse_args()
    loadEmbedings(args)
