import time

class Timer(object):
    def __init__(self):
        self.start = time.time()

    def get_time(self):
        return time.time() - self.start