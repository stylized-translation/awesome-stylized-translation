import os
import sys
import random
import numpy as np
import pandas as pd

from collections import OrderedDict
from tabulate import tabulate
from pandas import DataFrame
from time import gmtime, strftime


class Logger:
    def __init__(self, logger_name, iter_name, root, fmt=None, checkpoint_logs=None):
        self.with_header = True
        self.scalar_metrics = OrderedDict()
        self.fmt = fmt if fmt else dict()
        self.iter_name = iter_name
        self.checkpoint_logs = checkpoint_logs
        base = os.path.join(root, 'logs')
        if not os.path.exists(base): os.mkdir(base)

        self.path = '%s/%s' % (base, logger_name)

        self.logs = self.path + '.csv'
        self.output = self.path + '.out'

    def add_scalar(self, t, key, value):
        if key not in self.scalar_metrics:
            self.scalar_metrics[key] = []
        self.scalar_metrics[key] += [(t, value)]

    def new_table(self, start=True):
        self.with_header = start

    def iter_info(self, order=None, force_with_header=False):
        names = list(self.scalar_metrics.keys())
        if order:
            names = order
        values = [self.scalar_metrics[name][-1][1] for name in names]
        t = int(np.max([self.scalar_metrics[name][-1][0] for name in names]))
        fmt = ['%s'] + [self.fmt[name] if name in self.fmt else '.1f' for name in names]
        if self.with_header or force_with_header:
            print(tabulate([[t] + values], [self.iter_name] + names, floatfmt=fmt))
            self.with_header = False
        else:
            print(tabulate([[t] + values], [self.iter_name] + names, tablefmt='plain', floatfmt=fmt).split('\n')[1])

    def get_csv(self):
        result = None
        for key in self.scalar_metrics.keys():
            if result is None:
                result = DataFrame(self.scalar_metrics[key], columns=[self.iter_name, key]).set_index(self.iter_name)
            else:
                df = DataFrame(self.scalar_metrics[key], columns=[self.iter_name, key]).set_index(self.iter_name)
                result = result.join(df, how='outer')

        return result

    def save(self, save_out=False):
        result = None
        for key in self.scalar_metrics.keys():
            if result is None:
                result = DataFrame(self.scalar_metrics[key], columns=[self.iter_name, key]).set_index(self.iter_name)
            else:
                df = DataFrame(self.scalar_metrics[key], columns=[self.iter_name, key]).set_index(self.iter_name)
                result = result.join(df, how='outer')

        if save_out:
            with open(self.output, 'w') as f:
                f.write(tabulate(result, headers="keys"))
                f.flush()
            print('The output have been saved to: ' + self.path + '.out')

        if self.checkpoint_logs is None:
            result.to_csv(self.logs)
        else:
            pd.concat([self.checkpoint_logs.set_index(self.iter_name), result]).to_csv(self.logs)
        print('The log have been saved to: ' + self.path + '.csv')
