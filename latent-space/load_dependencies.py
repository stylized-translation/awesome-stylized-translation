import sys, os

def install(package):
    try:
        __import__(package)
    except:
        import subprocess
        subprocess.call([sys.executable, "-m", "pip", "install", package])

def loadDependencies():
    install("exman")
    install("tabulate")
    install("transformers")
    install("fasttext")
    install("deeppavlov")


if __name__ == '__main__':
    loadDependencies()
