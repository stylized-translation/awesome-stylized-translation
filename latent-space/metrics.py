from nltk.translate.bleu_score import corpus_bleu
from nltk.translate.gleu_score import corpus_gleu
import numpy as np

# based on nltk implementation
# https://www.nltk.org/api/nltk.translate.html#module-nltk.translate.gleu_score
# https://www.nltk.org/api/nltk.translate.html#module-nltk.translate.bleu_score

def __metrics_preprocess_translations(sentences):
    return [sentence.split() for sentence in sentences]

def __metrics_preprocess_references(sentences):
    return [[sentence.split()] for sentence in sentences]

def metrics_gleu_score(references, translations):
    """gleu score calculation

    Keyword arguments:
    translations -- ['string'] corpus of translated sentences
    references -- ['string'] corpus of referenced sentences

    returns score
    """
    return corpus_gleu(
        __metrics_preprocess_references(references),
        __metrics_preprocess_translations(translations)
    )

def metrics_bleu_score(references, translations):
    """bleu score calculation

    Keyword arguments:
    translations -- ['string'] corpus of translated sentences
    references -- ['string'] corpus of referenced sentences

    returns score
    """
    return corpus_bleu(
        __metrics_preprocess_references(references),
        __metrics_preprocess_translations(translations)
    )