import torch
import torch.nn as nn
from utils import infer_mask

class Encoder(nn.Module):
    def __init__(self, emb_size, hidden_size=256, style_size=8, content_size=128):
        super(Encoder, self).__init__()
        self.hidden_size = hidden_size
        
        self.gru = nn.GRUCell(emb_size, hidden_size)
        self.linear_s = nn.Linear(hidden_size, style_size)
        self.linear_c = nn.Linear(hidden_size, content_size)
        
    def forward(self, input, mask):
        batch_size = input.shape[0]
        state = torch.zeros(batch_size, self.hidden_size, device=input.device)
        
        # forward batch through gru cell
        for i in range(mask.shape[1]):
            next_state = self.gru(input[:, i], state)
            state = torch.where(mask[:, i].repeat([next_state.shape[1], 1]).T, next_state, state)
        
        style_out = self.linear_s(state)
        content_out = self.linear_c(state)
        return style_out, content_out


class Decoder(nn.Module):
    def __init__(self, emb_size, output_size, hidden_size=256, latent_size=136, decoder_type=2):
        super(Decoder, self).__init__()
        self.hidden_size = hidden_size
        self.decoder_type = decoder_type
        self.output_size = output_size
        
        if self.decoder_type == 1:
            in_gru_size, out_gru_size = emb_size + latent_size, hidden_size
        elif self.decoder_type == 2:
            in_gru_size, out_gru_size = emb_size, latent_size
        
        self.gru = nn.GRUCell(in_gru_size, out_gru_size)
        self.out = nn.Linear(out_gru_size, output_size)
        self.log_softmax = nn.LogSoftmax(dim=1)

    def forward(self, input, style_out, content_out):
        if self.decoder_type == 1:
            return self.forward1(input, style_out, content_out)
        return self.forward2(input, style_out, content_out)

    def forward1(self, input, style_out, content_out):
        """
        Returns [batch_size, T, vocab_size] tensor with predicted logits
        """
        latent_vector = torch.cat([style_out, content_out], dim=1)
        batch_size = input.shape[0]
        state = torch.zeros(batch_size, self.hidden_size-latent_vector.shape[1], device=input.device)
        state = torch.cat([state, latent_vector], dim=1)
        embs = input
        T = embs.shape[1]
        output = []
        for i in range(T):
            prev_token_embs = embs[:, i - 1] if i > 0 else torch.zeros_like(embs[:, i])
            input_gru = torch.cat([prev_token_embs, latent_vector], dim=1)
            state = self.gru(input_gru, state)
            logits = self.log_softmax(self.out(state))
            output.append(logits)

        return torch.stack(output, dim=2)

    def forward2(self, input, style_out, content_out):
        state = torch.cat([style_out, content_out], dim=1)
        batch_size = input.shape[0]
        T = input.shape[1]
        output = []
        for i in range(T):
            prev_token_embs = input[:, i - 1] if i > 0 else torch.zeros_like(input[:, i])
            state = self.gru(prev_token_embs, state)
            logits = self.log_softmax(self.out(state))
            output.append(logits)
            
        return torch.stack(output, dim=2)


class Autoencoder(nn.Module):
    def __init__(self, emb_size, output_size, style_size=8, content_size=128, hidden_size=256, decoder_type=2):
        super(Autoencoder, self).__init__()
        self.hidden_size = hidden_size
        self.encoder = Encoder(emb_size=emb_size, hidden_size=hidden_size, style_size=style_size, content_size=content_size)
        self.decoder = Decoder(emb_size=emb_size, hidden_size=hidden_size, output_size=output_size, decoder_type=decoder_type)

    def forward(self, input, mask, set_style=None):
        style, content = self.encoder(input, mask)
        if set_style is not None:
            style = set_style
        output = self.decoder(input, style, content)
        return style, content, output

class StyleTransferModel(nn.Module):
    def __init__(self, emb_size, output_size, num_styles, style_size=8, 
                 content_size=128, hidden_size=256, clf_hidden_size=32, decoder_type=2):
        super(StyleTransferModel, self).__init__()
        self.hidden_size = hidden_size
        self.autoencoder = Autoencoder(emb_size, output_size, style_size, content_size, hidden_size, decoder_type)
        
        self.ss_clf = nn.Sequential(nn.Linear(style_size, clf_hidden_size),
                                    nn.LeakyReLU(),
                                    nn.Linear(clf_hidden_size, num_styles))

        self.sc_clf = nn.Sequential(nn.Linear(style_size, clf_hidden_size),
                                    nn.LeakyReLU(),
                                    nn.Linear(clf_hidden_size, output_size))

        self.cs_clf = nn.Sequential(nn.Linear(content_size, clf_hidden_size),
                                    nn.LeakyReLU(),
                                    nn.Linear(clf_hidden_size, num_styles))

        self.cc_clf = nn.Sequential(nn.Linear(content_size, clf_hidden_size),
                                    nn.LeakyReLU(),
                                    nn.Linear(clf_hidden_size, output_size))

    def forward(self, input, mask, set_style=None):
        style, content, output = self.autoencoder(input, mask, set_style)
        ss_pred = self.ss_clf(style)
        sc_pred = self.sc_clf(style)
        cs_pred = self.cs_clf(content)
        cc_pred = self.cc_clf(content)
        return style, content, output, ss_pred, sc_pred, cs_pred, cc_pred
