import exman
from trainer import StyleTransferTrainer
#from data_handler import BertTextHandler, iterate_pickles
from data_handler import BertNoAttentionTextHandler, NoAttentionBertDataset
from torch.utils.data import Dataset, DataLoader
from model import Autoencoder, StyleTransferModel
import os
import utils

name = os.popen('whoami').read().strip()
data_path = os.path.join("/data", 'avbolychev')
#embs_path = os.path.join(data_path, 'embeddings')

# train_csv_id = '1T9MLSLIIAfslcT_Od6CgEWPZg3hr70RY'
# val_csv_id = '11GXyAhbE4jyW1DSnfXss5WcCOJE_xQ_H'
# train_csv_path = os.path.join(data_path, 'train.csv')
# val_csv_path = os.path.join(data_path, 'val.csv')

exman_path = os.path.join(data_path, name + '_experiments', 'exman')

parser = exman.ExParser(root=exman.simpleroot(exman_path))
parser.add_argument('--batch_size', type=int, default=50)
parser.add_argument('--lr', type=float, default=2e-4)
parser.add_argument('--beta1', type=float, default=0.5)
parser.add_argument('--beta2', type=float, default=0.999)
parser.add_argument('--n_epoch', type=int, default=300)
parser.add_argument('--continue_from', type=str, default=None)
parser.add_argument('--checkpoint_every', type=int, default=25)
parser.add_argument('--gpu', type=int, default=None)
parser.add_argument('--seed', type=int, default=42)
parser.add_argument('--data_type', type=str, default='tweets')
#parser.add_argument('--embeddings_path', type=str, default=embs_path)
parser.add_argument('--lambda_ss', type=float, default=10.)
parser.add_argument('--lambda_sc', type=float, default=0.03)
parser.add_argument('--lambda_cs', type=float, default=1.)
parser.add_argument('--lambda_cc', type=float, default=3.)
parser.add_argument('--decoder_type', type=int, default=2)

if __name__ == '__main__':
    args = parser.parse_args()
    with args.safe_experiment:
        utils.set_random_seed(args.seed)
        if args.data_type == 'tweets':
            train_csv_path = os.path.join(data_path, 'train.csv')
            val_csv_path = os.path.join(data_path, 'val.csv')
        else:
            train_csv_path = os.path.join(data_path, 'tales-news-train.csv')
            val_csv_path = os.path.join(data_path, 'tales-news-val.csv')
        
        text_handler = BertNoAttentionTextHandler()
        
        
#         for csv_id, path in [(train_csv_id, train_csv_path), (val_csv_id, val_csv_path)]:
#             if not os.path.exists(path):
#                 print('Downloading', path)
#                 wget_command = get_gdrive_wget_command(file_id=csv_id, save_to_path=path)
#                 result_command = os.system(wget_command)
#                 print('Command\n', wget_command, '\nreturned', result_command)
        
        train_dataset = NoAttentionBertDataset(text_handler, train_csv_path, random_order=False)
        val_dataset = NoAttentionBertDataset(text_handler, val_csv_path, random_order=True)
        
        train_loader = DataLoader(train_dataset, batch_size=args.batch_size, shuffle=True)
        val_loader = DataLoader(val_dataset, batch_size=args.batch_size, shuffle=False)
        
        model = StyleTransferModel(emb_size=text_handler.get_emb_size(), 
                                   num_styles=2, # TODO:fix
                                   output_size=text_handler.get_vocab_size(),
                                   decoder_type=args.decoder_type)
        
        style_transfer_trainer = StyleTransferTrainer(model, 
                                                      lr=args.lr, 
                                                      beta1=args.beta1,
                                                      beta2=args.beta2, 
                                                      n_epoch=args.n_epoch, 
                                                      lambda_ss=args.lambda_ss,
                                                      lambda_sc=args.lambda_sc,
                                                      lambda_cs=args.lambda_cs,
                                                      lambda_cc=args.lambda_cc, 
                                                      gpu=args.gpu, 
                                                      root=args.root, 
                                                      continue_from=args.continue_from,
                                                      checkpoint_every=args.checkpoint_every,
                                                      random_seed=args.seed)
        
        style_transfer_trainer.train(train_loader, val_loader, text_handler)
