import fasttext
import numpy as np

class Embedder:
    def __init__(self, max_sentence_length, model_path = None, corpus_path=None, pad_ix=0., emb_size=100):
        # Arguments:
        #   max_sentence_length: maximum sentence length int
        #   model_path: path to fasttext model str, if None model will train on corpus_path
        #   corpus_path: path for data to train fasttext model
        #   emb_size: size of embeddings int (only if model_path None)
        
        if model_path:
            self.model = fasttext.load_model(model_path)
            self.emb_size = self.model.get_dimension() # just for instance
        else:
            if corpus_path:
                self.model = self.train_model(corpus_path, emb_size)
                self.emb_size = emb_size
        self.max_sentence_length = max_sentence_length
        self.pad = np.array([pad_ix] * self.emb_size)
        
    def save_model(self, model_path):
        self.model.save_model()

    def train_model(self, corpus_path, emb_size):
        return fasttext.train_unsupervised(corpus_path, dim=emb_size)

    def _prepare_string(self, string):
        string = string.lower()
        string = string.replace('ё', 'е')
        string = re.sub(r'[^а-я ]+', ' ', string)
        string = string.split()[:self.max_sentence_length]
        string = string + ['PADDING'] * (self.max_sentence_length - len(string))
        res = []
        for word in string:
            if word == 'PADDING':
                res.append(self.pad)
                continue
            res.append(self.model[word])
        return np.array(res)

    def __call__(self, input):
        # Arguments:
        #   input: batch of sentences
        # Output:
        #   np array: padded embedded input with shape (len(input), max_sentence_length, emb_size)
        return np.array([self._prepare_string(x) for x in input])