import os
import torch
import utils
from metrics import metrics_bleu_score
from plots import plot_tSNE_and_save
from timer import Timer
from logger import Logger
import numpy as np
import torch.nn.functional as F
from distutils.dir_util import copy_tree

from utils import infer_mask, get_bow_vector
from loss import ae_loss, cross_entropy_loss, negative_entropy_loss


class StyleTransferTrainer:
    def __init__(self,  model, lr, beta1, beta2, n_epoch,
                 lambda_ss, lambda_sc, lambda_cs, lambda_cc,
                 gpu, root, continue_from, checkpoint_every, random_seed):
        self.root = root
        self.n_epoch = n_epoch
        self.checkpoint_every = checkpoint_every
        self.gpu = gpu
        self.lr = lr
        self.beta1 = beta1
        self.beta2 = beta2
        self.continue_from = continue_from
        self.random_seed = random_seed
    
        if gpu is None:
            self.device = 'cpu'
        else:
            self.device = 'cuda:' + str(gpu)

        self.model = model.to(self.device)

        self.iter_id = 1
        self.first_epoch = 1
        
        self.lambda_ss = lambda_ss
        self.lambda_sc = lambda_sc
        self.lambda_cs = lambda_cs
        self.lambda_cc = lambda_cc
        
        self.encoder_opt = torch.optim.Adam(self.model.autoencoder.encoder.parameters(), 
                                            lr=lr, betas=(beta1,beta2))
        self.decoder_opt = torch.optim.Adam(self.model.autoencoder.decoder.parameters(), 
                                            lr=lr, betas=(beta1,beta2))
        self.ss_clf_opt = torch.optim.Adam(self.model.ss_clf.parameters(), 
                                            lr=lr, betas=(beta1,beta2))
        self.sc_clf_opt = torch.optim.Adam(self.model.sc_clf.parameters(), 
                                            lr=lr, betas=(beta1,beta2))
        self.cs_clf_opt = torch.optim.Adam(self.model.cs_clf.parameters(), 
                                            lr=lr, betas=(beta1,beta2))
        self.cc_clf_opt = torch.optim.Adam(self.model.cc_clf.parameters(), 
                                            lr=lr, betas=(beta1,beta2))
        
        iter_logs, epoch_logs = None, None
        if continue_from is not None:
            epoch, iter_id, experiment_path, checkpoint_path, iter_logs, epoch_logs =\
                utils.get_checkpoint_info(continue_from)
            self.iter_id = iter_id + 1
            self.first_epoch = epoch + 1

            print('Loading checkpoint from', checkpoint_path)
            checkpoint = torch.load(checkpoint_path, map_location=self.device)
            self.model.load_state_dict(checkpoint['model'])
            self.encoder_opt.load_state_dict(checkpoint['encoder_opt'])
            self.decoder_opt.load_state_dict(checkpoint['decoder_opt'])
            self.ss_clf_opt.load_state_dict(checkpoint['ss_clf_opt'])
            self.sc_clf_opt.load_state_dict(checkpoint['sc_clf_opt'])
            self.cs_clf_opt.load_state_dict(checkpoint['cs_clf_opt'])
            self.cc_clf_opt.load_state_dict(checkpoint['cc_clf_opt'])
            checkpoint = None

            for name in ['styles_labels_stacked', 'TSNE-plots']:
                path = os.path.join(experiment_path, name)
                if os.path.exists(path):
                    print('Loading', name, 'from', path)
                    if not os.path.exists(os.path.join(self.root, name)):
                        os.mkdir(os.path.join(self.root, name))
                    copy_tree(path, os.path.join(self.root, name))
        
        self.iter_logger = Logger(logger_name='iter_logs',
                                  iter_name='iteration',
                                  root=self.root,
                                  checkpoint_logs=iter_logs,
                                  fmt={
                                      'time': '.2f',
                                  })

        self.epoch_logger = Logger(logger_name='epoch_logs',
                                   iter_name='epoch',
                                   root=self.root,
                                   checkpoint_logs=epoch_logs,
                                   fmt={
                                       'epoch time': '.2f',
                                       'avg iter time': '.2f',
                                   })
        
        self.optimizers = [self.encoder_opt, self.decoder_opt, self.ss_clf_opt,
                           self.sc_clf_opt, self.cs_clf_opt, self.cc_clf_opt]
        
    def train_epoch(self, epoch, train_loader):
        self.model.train()

        iter_times = []
        self.iter_logger.new_table()  # for pretty output

        for batch_idx, batch in enumerate(train_loader):
            iter_timer = Timer()

            embs, ids, labels = batch['embeddings'], batch['BOI'], batch['labels']
            embs, ids, labels = embs.to(self.device), ids.to(self.device), labels.to(self.device)

            mask = infer_mask(ids, eos_ix=self.pad_id_symbol)
            style, content, output, ss_pred, sc_pred, cs_pred, cc_pred = self.model(embs, mask)
            bow_vectors = get_bow_vector(ids, self.vocab_size, self.pad_id_symbol)

            ae = ae_loss(output, ids, mask)
            ss_crossentropy = self.lambda_ss * F.cross_entropy(ss_pred, labels)
            sc_crossentropy = cross_entropy_loss(sc_pred, bow_vectors)
            cs_crossentropy = F.cross_entropy(cs_pred, labels)
            cc_crossentropy = self.lambda_cc * cross_entropy_loss(cc_pred, bow_vectors)

            sc_adversarial = self.lambda_sc * negative_entropy_loss(sc_pred)
            cs_adversarial = self.lambda_cs * negative_entropy_loss(cs_pred)

            for optimizer in self.optimizers:
                optimizer.zero_grad()
            ae.backward(retain_graph=True)
            self.encoder_opt.step()
            self.decoder_opt.step()

            for optimizer in self.optimizers:
                optimizer.zero_grad()
            ss_crossentropy.backward(retain_graph=True)
            self.encoder_opt.step()
            self.ss_clf_opt.step()

            for optimizer in self.optimizers:
                optimizer.zero_grad()
            sc_crossentropy.backward(retain_graph=True)
            self.sc_clf_opt.step()

            for optimizer in self.optimizers:
                optimizer.zero_grad()
            cs_crossentropy.backward(retain_graph=True)
            self.cs_clf_opt.step()

            for optimizer in self.optimizers:
                optimizer.zero_grad()
            cc_crossentropy.backward(retain_graph=True)
            self.encoder_opt.step()
            self.cc_clf_opt.step()

            for optimizer in self.optimizers:
                optimizer.zero_grad()
            sc_adversarial.backward(retain_graph=True)
            self.encoder_opt.step()

            for optimizer in self.optimizers:
                optimizer.zero_grad()
            cs_adversarial.backward()
            self.encoder_opt.step()

            iter_duration = iter_timer.get_time()
            iter_times.append(iter_duration)
            self.iter_logger.add_scalar(self.iter_id, 'epoch', epoch)
            self.iter_logger.add_scalar(self.iter_id, 'batch_idx', batch_idx)
            self.iter_logger.add_scalar(self.iter_id, 'ae_loss', ae.item())
            self.iter_logger.add_scalar(self.iter_id, 'ss_ce', ss_crossentropy.item())
            self.iter_logger.add_scalar(self.iter_id, 'sc_ce', sc_crossentropy.item())
            self.iter_logger.add_scalar(self.iter_id, 'cs_ce', cs_crossentropy.item())
            self.iter_logger.add_scalar(self.iter_id, 'cc_ce', cc_crossentropy.item())
            self.iter_logger.add_scalar(self.iter_id, 'sc_ne', sc_adversarial.item())
            self.iter_logger.add_scalar(self.iter_id, 'cs_ne', cs_adversarial.item())
            self.iter_logger.add_scalar(self.iter_id, 'time', iter_duration)
            self.iter_id += 1

            if batch_idx % 100 == 0:
                #self.iter_logger.save()
                self.iter_logger.iter_info(order=['batch_idx', 'ae_loss', 'ss_ce', 
                                                  'sc_ce', 'cs_ce', 'cc_ce', 'sc_ne',
                                                  'cs_ne', 'time'])
 #           if batch_idx % 5 == 0 and batch_idx > 0:
 #               break

        return np.mean(iter_times)
    
    def validate_epoch(self, epoch, val_loader, text_handler):
        avg_ae_loss = 0
        avg_ss_ce = 0
        avg_sc_ce = 0
        avg_cs_ce = 0
        avg_cc_ce = 0
        avg_sc_ne = 0
        avg_cs_ne = 0
        avg_time_iter = 0

        val_logger = Logger(logger_name='val_logs',
                            iter_name='iteration',
                            root=self.root,
                            checkpoint_logs=None,
                                  fmt={
                                      'time': '.2f',
                                  })

        self.model.eval()
        styles, contents, outputs, ss_preds, sc_preds, cs_preds, cc_preds = [], [], [], [], [], [], []
        out_texts = []
        original_texts = []
        concat_labels = []


        for batch_idx, batch in enumerate(val_loader):
            iter_timer = Timer()

            embs, ids, labels = batch['embeddings'], batch['BOI'], batch['labels']
            embs, ids, labels = embs.to(self.device), ids.to(self.device), labels.to(self.device)
            concat_labels.append(labels.detach().cpu().data.numpy())

            mask = infer_mask(ids, eos_ix=text_handler.get_pad_id_symbol())
            style, content, output, ss_pred, sc_pred, cs_pred, cc_pred = self.model(embs, mask)
            styles.append(style.detach().cpu().data.numpy())
            contents.append(content.detach().cpu().data.numpy())
            out_ids = torch.argmax(output, dim=1).cpu().detach().numpy()
            out_texts += text_handler.transform_ids_to_str(out_ids)
            original_texts += text_handler.transform_ids_to_str(ids.detach().cpu().numpy())
            bow_vectors = get_bow_vector(ids, text_handler.get_vocab_size(), 
                                         text_handler.get_pad_id_symbol())

            ae = ae_loss(output, ids, mask)
            ss_crossentropy = F.cross_entropy(ss_pred, labels)
            sc_crossentropy = cross_entropy_loss(sc_pred, bow_vectors)
            cs_crossentropy = F.cross_entropy(cs_pred, labels)
            cc_crossentropy = cross_entropy_loss(cc_pred, bow_vectors)

            sc_adversarial = negative_entropy_loss(sc_pred)
            cs_adversarial = negative_entropy_loss(cs_pred)
            iter_duration = iter_timer.get_time()

            avg_time_iter += (iter_duration - avg_time_iter) / (batch_idx + 1)
            avg_ae_loss += (ae.item() - avg_ae_loss) / (batch_idx + 1)
            avg_ss_ce += (ss_crossentropy.item() - avg_ss_ce) / (batch_idx + 1)
            avg_sc_ce += (sc_crossentropy.item() - avg_sc_ce) / (batch_idx + 1)
            avg_cs_ce += (cs_crossentropy.item() - avg_cs_ce) / (batch_idx + 1)
            avg_cc_ce += (cc_crossentropy.item() - avg_cc_ce) / (batch_idx + 1)
            avg_sc_ne += (sc_adversarial.item()  - avg_sc_ne) / (batch_idx + 1)
            avg_cs_ne += (cs_adversarial.item()  - avg_cs_ne) / (batch_idx + 1)

            iter_duration = iter_timer.get_time()
            val_logger.add_scalar(batch_idx, 'epoch', epoch)
            val_logger.add_scalar(batch_idx, 'batch_idx', batch_idx)
            val_logger.add_scalar(batch_idx, 'ae_loss', ae.item())
            val_logger.add_scalar(batch_idx, 'ss_ce', ss_crossentropy.item())
            val_logger.add_scalar(batch_idx, 'sc_ce', sc_crossentropy.item())
            val_logger.add_scalar(batch_idx, 'cs_ce', cs_crossentropy.item())
            val_logger.add_scalar(batch_idx, 'cc_ce', cc_crossentropy.item())
            val_logger.add_scalar(batch_idx, 'sc_ne', sc_adversarial.item())
            val_logger.add_scalar(batch_idx, 'cs_ne', cs_adversarial.item())
            val_logger.add_scalar(batch_idx, 'time', iter_duration)

            if batch_idx % 100 == 0:
                val_logger.iter_info(order=['batch_idx', 'ae_loss', 'ss_ce', 
                                                  'sc_ce', 'cs_ce', 'cc_ce', 'sc_ne',
                                                  'cs_ne', 'time'])
                
#            if batch_idx % 100 == 0 and batch_idx > 0:
#                break
        
        
        bleu = metrics_bleu_score(original_texts, out_texts)
        styles = np.concatenate(styles, axis=0)
        contents = np.concatenate(contents, axis=0)
        labels =  np.concatenate(concat_labels, axis=0)
        
        styles_labels_stacked_path = os.path.join(self.root, 'styles_labels_stacked')
        
        if not os.path.exists(styles_labels_stacked_path):
            os.mkdir(styles_labels_stacked_path)

        styles_labels_stacked = np.hstack((styles, labels[:, None]))
        styles_labels_stacked_path = os.path.join(styles_labels_stacked_path, 'styles_labels_stacked_epoch_{0:05d}.npy'.format(epoch))
        np.save(styles_labels_stacked_path, styles_labels_stacked)

        print('Styles labels stacked has been saved to', styles_labels_stacked_path)

        N = len(styles)

        samples_ids = np.array(np.arange(0, N // 5), dtype=int)
        
        styles = styles[samples_ids]
        contents = contents[samples_ids]
        labels = labels[samples_ids] 
        
        tsne_path = os.path.join(self.root, 'TSNE-plots')
        if not os.path.exists(tsne_path):
            os.mkdir(tsne_path)
        
        print('Plotting styles TSNE...')
        styles_path = os.path.join(tsne_path, 'tsne_styles_epoch_{0:05d}.png'.format(epoch))
        plot_tSNE_and_save(styles, labels, savepath=styles_path, figname='styles', random_state=self.random_seed)
        print('Style plot has been saved to', styles_path)
 
        print('Plotting contents TSNE...')
        contents_path = os.path.join(tsne_path, 'tsne_contents_epoch_{0:05d}.png'.format(epoch))
        plot_tSNE_and_save(contents, labels, savepath=contents_path, figname='contents', random_state=self.random_seed)
        print('Contents plot has been saved to', contents_path)

        return avg_ae_loss, avg_ss_ce, avg_sc_ce, avg_cs_ce, avg_cc_ce, avg_sc_ne, avg_cs_ne, avg_time_iter, bleu
    
    def train(self, train_loader, val_loader, text_handler):
        self.pad_id_symbol = text_handler.get_pad_id_symbol()
        self.vocab_size = text_handler.get_vocab_size()
        
        
        checkpoints_dir = os.path.join(self.root, 'checkpoints')
        if not os.path.exists(checkpoints_dir):
            os.mkdir(checkpoints_dir)

        for epoch in range(self.first_epoch, self.n_epoch + 1):
            print("\n============\n", epoch, "started")
            epoch_timer = Timer()
            avg_time_iter = self.train_epoch(epoch, train_loader)
            epoch_duration = epoch_timer.get_time()
            print("\n============\n", epoch, "trained")
            self.epoch_logger.add_scalar(epoch, 'epoch time', epoch_duration)
            self.epoch_logger.add_scalar(epoch, 'avg iter time', avg_time_iter)
            print("\n============\n", epoch, "validating...")
            avg_ae_loss, avg_ss_ce, avg_sc_ce, avg_cs_ce,\
            avg_cc_ce, avg_sc_ne, avg_cs_ne, avg_time_iter, bleu = self.validate_epoch(epoch, val_loader, text_handler)
            print("\n============\n", epoch, "validated")
            self.epoch_logger.add_scalar(epoch, 'val_ae_loss', avg_ae_loss)
            self.epoch_logger.add_scalar(epoch, 'val_ss_ce', avg_ss_ce)
            self.epoch_logger.add_scalar(epoch, 'val_cs_ce', avg_cs_ce)
            self.epoch_logger.add_scalar(epoch, 'val_sc_ce', avg_sc_ce)
            self.epoch_logger.add_scalar(epoch, 'val_cc_ce', avg_cc_ce)
            self.epoch_logger.add_scalar(epoch, 'val_cs_ne', avg_cs_ne)
            self.epoch_logger.add_scalar(epoch, 'val_sc_ne', avg_sc_ne)
            self.epoch_logger.add_scalar(epoch, 'bleu', bleu)
            
            if epoch % self.checkpoint_every == 0 or epoch == self.n_epoch:
                epoch_dir = os.path.join(checkpoints_dir, 'epoch_{0:05d}'.format(epoch))
                print('Saving checkpoints epoch to', epoch_dir)

                torch.save({
                    'model': self.model.state_dict(),
                    'encoder_opt': self.encoder_opt.state_dict(),
                    'decoder_opt': self.decoder_opt.state_dict(),
                    'ss_clf_opt': self.ss_clf_opt.state_dict(),
                    'sc_clf_opt': self.sc_clf_opt.state_dict(),
                    'cs_clf_opt': self.cs_clf_opt.state_dict(),
                    'cc_clf_opt': self.cc_clf_opt.state_dict()
                }, epoch_dir)

            self.iter_logger.save()
            self.epoch_logger.save(save_out=True)

            print('Epoch stats:')
            self.epoch_logger.iter_info(force_with_header=True)

