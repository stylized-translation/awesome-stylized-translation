import torch
import torch.nn as nn
import torch.nn.functional as F
from utils import infer_mask

def ae_loss(logits, input, mask):
    #log_probs = torch.log_softmax(logits, dim=1).to(input.device)
    log_probs = logits
    targets = F.one_hot(input.type(torch.long), num_classes=log_probs.shape[1]).transpose(1, 2)
    targets = targets.to(input.device)
    
    return torch.mean(torch.sum(torch.sum(-log_probs * targets, dim=1) * mask, dim=1))

def vae_loss(out_logits, input):
    pass

def cross_entropy_loss(pred_logits, target):
    log_probs = F.log_softmax(pred_logits, dim=1)
    return -torch.mean(torch.sum(log_probs * target, dim=1))

def negative_entropy_loss(logits): # adversarial loss for classifiers
    log_probs = torch.log_softmax(logits, dim=1)
    probs = torch.exp(log_probs)
    
    return torch.mean(torch.sum(probs * log_probs, dim=1))